class QuadBuffer 
{
  constructor(pushQuadCallback) 
  {
    this._pushQuadCallback = pushQuadCallback;

    function Vertex() {
      this.position = [0, 0, 0];
      this.color = [0, 0, 0, 0];
      this.texCoords = [0, 0];
    }

    this._vertices = [ 
      new Vertex(),
      new Vertex(),
      new Vertex(),
      new Vertex()
    ];

    this._currentVertex = 0;
    this._renderStyle = 0;

    this._scale = 1;
    this._camera = null;
  }

  initialize(maxNumVertices, texChannels, indices, maxNumRenderCalls) 
  {
  }

  setScale(value) {
    this._scale = value;
  }

  setCamera(camera) {
    this._camera = camera;
  }

  beforeQuad(renderStyle) 
  {
    this._renderStyle = renderStyle;
  }

  pushVertex(vertex) 
  {
    const v = this._vertices[this._currentVertex];

    v.position[0] = vertex.position[0] * this._scale - this._camera.scrollX;
    v.position[1] = vertex.position[1] * this._scale - this._camera.scrollY;
    v.position[2] = vertex.position[2] * this._scale;

    v.color[0] = vertex.color[0];
    v.color[1] = vertex.color[1];
    v.color[2] = vertex.color[2];
    v.color[3] = vertex.color[3];

    v.texCoords[0] = vertex.texCoords[0][0];
    v.texCoords[1] = vertex.texCoords[0][1];

    ++this._currentVertex;

    if (this._currentVertex == 4)
    {
      this._pushQuadCallback(this._vertices, this._renderStyle);
      this._currentVertex = 0;
    }
  }

  pushRenderCall(rc) 
  {
  }

  cleanup() 
  {
  }
}

export {
  QuadBuffer
}