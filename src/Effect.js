import * as Phaser from 'phaser';
import { QuadBuffer } from './QuadBuffer';
import { Pause, materialIndexToBlendMode } from './const';

/**
 * Represents an instance of NeutrinoParticles effect on the scene.
 * 
 * The class is a wrapper around instance of exported NeutrinoParticles effect. It
 * introduces everything to place effect on a Phaser scene.
 *
 * Usually you won't create objects of this class manually, rather use 
 * {@link Phaser.GameObjects.GameObjectCreator.neutrino|scene.make.neutrino()} or
 * {@link Phaser.GameObjects.GameObjectFactory.neutrino|scene.add.neutrino()} to create and add effect
 * to the scene.
 * 
 * @memberof PhaserNeutrino
 * @alias Effect
 * 
 * @extends Phaser.GameObjects.GameObject
 * @extends Phaser.GameObjects.Components.Alpha
 * @extends Phaser.GameObjects.Components.BlendMode
 * @extends Phaser.GameObjects.Components.Depth
 * @extends Phaser.GameObjects.Components.Pipeline
 * @extends Phaser.GameObjects.Components.Transform
 * @extends Phaser.GameObjects.Components.ScrollFactor
 * 
 * @param {EffectModel} effectModel A model which is used to instantiate and simulate effect.
 * @param {Object} [options={}] Options
 * @param {number[]} [options.position=[0, 0, 0]] [x, y, z] Location of the effect on a scene.
 * @param {number} [options.angle=0] Rotation of the effect in degrees.
 * @param {number} [options.scale=1]  Scale of the effect.
 * @param {Pause} [options.pause=BEFORE_UPDATE_RENDER] Type of pause on the start.
 * @param {boolean} [options.generatorsPaused=false] Generators paused on the start.
 */

class Effect extends new Phaser.Class({
	Extends: Phaser.GameObjects.GameObject,
	Mixins: [
		Phaser.GameObjects.Components.Alpha,
		Phaser.GameObjects.Components.BlendMode,
		Phaser.GameObjects.Components.Depth,
		Phaser.GameObjects.Components.Pipeline,
		Phaser.GameObjects.Components.Transform,
		Phaser.GameObjects.Components.ScrollFactor
	]
})
{
	constructor(effectModel, scene, options) {
		options = Object.assign({
			position: [0, 0, 0],
			angle: 0,
			scale: 1,
			pause: Pause.BEFORE_UPDATE_OR_RENDER,
			generatorsPaused: false
		}, options || {});

		super(scene, 'Neutrino');

		if (!effectModel) {
			console.warn('Invalid effect model provided.');
		}

		/**
		 * The NeutrinoParticles plugin instance.
		 * @type {GamePlugin}
		 */
		this.gamePlugin = scene.plugins.get('neutrino');

		/**
		 * The {@link EffectModel} used as model for this effect instance.
		 */
		this.effectModel = effectModel;

		/**
     	 * {@link neutrinoparticles.js} effect instance
     	 */
		this.neutrinoEffect = null;

		this.setPosition(options.position[0], options.position[1], options.position[2]);

		this.angle = options.angle;
		this.scaleX = options.scale;
		this.scaleY = options.scale;

		this._worldPosition = new Phaser.Math.Vector2();
		this._worldScaledPosition = [0, 0, 0];
		this._worldAngle = 0;
		this._worldScale = 1;
		this._tempMatrix1 = new Phaser.GameObjects.Components.TransformMatrix();
		this._tempMatrix2 = new Phaser.GameObjects.Components.TransformMatrix();

		this._updateWorldTransform();

		this._unpauseOnUpdateRender = (options.pause === Pause.BEFORE_UPDATE_OR_RENDER);

		this.initPipeline("NeutrinoParticles");

		const startupOptions = {
			paused: options.pause !== Pause.NO,
			generatorsPaused: options.generatorsPaused
		};

		if (this.effectModel) {
			if (this.gamePlugin.canvasRenderer) {
				this.neutrinoEffect = this.effectModel.neutrinoEffectModel.createCanvas2DInstance(
					this._renderPosition(), this._renderRotation(), startupOptions);
				this.neutrinoEffect.textureDescs = this.effectModel.textureImageDescs;
			}
			else {
				this._quadBuffer = new QuadBuffer((vertices, renderStyle) => {
					this._pushQuad(vertices, renderStyle);
				});

				this.neutrinoEffect = this.effectModel.neutrinoEffectModel.createWGLInstance(
					this._renderPosition(), this._renderRotation(), this._quadBuffer,
					startupOptions);
				this.neutrinoEffect.texturesRemap = this.effectModel.texturesRemap;
			}
		}
	}

	get ready() {
		return this.effectModel && this.effectModel.ready;
	}

	preUpdate(time, ms) {
		if (!this.ready)
			return;

		if (!this._checkUnpauseOnUpdateRender())
			this._updateWorldTransform();


		this.neutrinoEffect.update(ms / 1000.0, this._renderPosition(), this._renderRotation());
	}

	renderCanvas(renderer, src, interpolationPercentage, camera, parentMatrix) {
		if (!this.ready)
			return;

		this._checkUnpauseOnUpdateRender();

		const context = renderer.currentContext;

		const scale = (this.scaleX + this.scaleY) * 0.5;
		const localMatrix = this._tempMatrix1;
		localMatrix.applyITRS(-camera.scrollX, -camera.scrollY, 0, scale, scale);

		const camMatrix = this._tempMatrix2;
		camMatrix.copyFrom(camera.matrix);
		camMatrix.multiply(localMatrix);
		camMatrix.setToContext(context);

		this.neutrinoEffect.draw(context);
	};

	renderWebGL(renderer, container, interpolationPercentage, camera, parentMatrix) {
		if (!this.ready)
			return;

		this._checkUnpauseOnUpdateRender();

		renderer.setPipeline(this.pipeline, this);

		this._quadBuffer.setScale(this._worldScale);
		this._quadBuffer.setCamera(camera);
		this.neutrinoEffect.fillGeometryBuffers([1, 0, 0], [0, -1, 0], [0, 0, -1]);
	}

	/**
	   * Restarts the effect.
	   * 
	   * @param {Array} position A new position of the effect or null to keep the same position.
	   * @param {number} rotation A new rotation of the effect of null to keep the same rotation.
	   */
	restart(options) {
		if (!this.ready)
			return;

		if (options) {
			if (options.position) {
				this.position.x = options.position[0];
				this.position.y = options.position[1];
				this.positionZ = options.position[2];
			}

			if (options.angle) {
				this.angle = options.angle;
			}
			else if (options.rotation) {
				this.rotation = options.rotation;
			}
		}

		this._updateWorldTransform();

		this.neutrinoEffect.restart(this._renderPosition(), this._renderRotation());
	}

	/**
	 * Instant change of position/rotation of the effect. 
	 * 
	 * While {@link Effect#update} changes position 
	 * of the effect smoothly interpolated, this function will prevent such smooth trail
	 * from previous position and "teleport" effect instantly.
	 * 
	 */
	resetPosition(options) {
		if (!this.ready)
			return;

		if (options) {
			if (options.position) {
				this.x = options.position[0];
				this.y = options.position[1];
				this.z = options.position[2];
			}

			if (options.angle) {
				this.angle = options.angle;
			}
			else if (options.rotation) {
				this.rotation = options.rotation;
			}
		}

		this._updateWorldTransform();
		this.neutrinoEffect.resetPosition(this._renderPosition(), this._renderRotation());
	}

	/** Pauses the effect. All active particles will freeze. */
	pause() {
		if (this.ready)
			this.neutrinoEffect.pauseAllEmitters();
	}

	/** Unpauses the effect. */
	unpause() {
		if (this.ready)
			this.neutrinoEffect.unpauseAllEmitters();
	}

	get paused() {
		if (this.ready)
			return this.neutrinoEffect.areAllEmittersPaused();
	}

	/** 
	 * Pauses all generators in the effect. No new particles will be generated.
	 * All current particles will continue to update.
	 */
	pauseGenerators() {
		if (this.ready)
			this.neutrinoEffect.pauseGeneratorsInAllEmitters();
	}

	/** Unpauses generators in the effect. */
	unpauseGenerators() {
		if (this.ready)
			this.neutrinoEffect.unpauseGeneratorsInAllEmitters();
	}

	get generatorsPaused() {
		if (this.ready)
			return this.neutrinoEffect.areGeneratorsInAllEmittersPaused();
	}

	setPropertyInAllEmitters(name, value) {
		if (this.ready)
			this.neutrinoEffect.setPropertyInAllEmitters(name, value);
	}

	getNumParticles() {
		return this.ready ? this.neutrinoEffect.getNumParticles() : 0;
	}

	_updateWorldTransform() {

		let computeWorldTransform = (parent, result) => {
			if (!parent)
				return;

			const parentLocalMatrix = this._tempMatrix2;
			parentLocalMatrix.applyITRS(parent.x, parent.y, parent.rotation, parent.scaleX, parent.scaleY);
			parentLocalMatrix.multiply(result.matrix, result.matrix);
			result.angle += parent.angle;
			result.scale *= (parent.scaleX + parent.scaleY) * 0.5;
			computeWorldTransform(parent.parentContainer, result);
		}

		const transform =
		{
			matrix: this._tempMatrix1.loadIdentity(),
			angle: this.angle,
			scale: (this.scaleY + this.scaleY) * 0.5
		};
		computeWorldTransform(this.parentContainer, transform);

		transform.matrix.transformPoint(this.x, this.y, this._worldPosition);
		this._worldRotation = this.gamePlugin.neutrino.axisangle2quat_([0, 0, 1], transform.angle);
		this._worldScale = transform.scale;
		this._worldScaledPosition[0] = this._worldPosition.x / this._worldScale;
		this._worldScaledPosition[1] = this._worldPosition.y / this._worldScale;
		this._worldScaledPosition[2] = this.z / this._worldScale;
	}

	_renderPosition() {
		return this._worldScaledPosition;
	}

	_renderRotation() {
		return this._worldRotation;
	}

	_pushQuad(vertices, renderStyleIndex) {
		const renderStyle = this.neutrinoEffect.model.renderStyles[renderStyleIndex];
		const texIndex = renderStyle.textureIndices[0];
		const textureFrame = this.effectModel.textureFrames[texIndex];
		const material = this.neutrinoEffect.model.materials[renderStyle.materialIndex];

		this.pipeline.pushQuad(vertices,
			textureFrame.glTexture,
			materialIndexToBlendMode[material]);
	}

	_checkUnpauseOnUpdateRender() {
		if (!this._unpauseOnUpdateRender)
			return false;

		this.resetPosition();
		this.unpause();
		this._unpauseOnUpdateRender = false;
		return true;
	}
}

export {
	Effect
}