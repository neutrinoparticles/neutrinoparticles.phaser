import * as Phaser from 'phaser';
import { EffectModel } from './EffectModel';

class File extends Phaser.Loader.File
{
    constructor(loader, config)
    {
        const { key, url, options, xhrSettings } = config;

        super(loader, {
            type: 'binary',
            cache: loader.cacheManager.binary,
            extension: 'js',
            responseType: 'text',
            key,
            url,
            xhrSettings
        });

        this._options = options;
    }

    onProcess()
    {
        this.state = Phaser.Loader.FILE_PROCESSING;
        const text = this.xhrLoader.response;

        this.data = new EffectModel(this.loader.scene, text, this._options);

        this.onProcessComplete();
    }
}

export {
    File
}