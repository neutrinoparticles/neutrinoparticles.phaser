const vertexShader = `
/* NeutrinoParticles Vertex Shader */

attribute vec3 aVertexPosition;
attribute vec2 aTextureCoord;
attribute vec4 aColor;

uniform mat4 uProjectionMatrix;
uniform mat4 uViewMatrix;
uniform mat4 uModelMatrix;

varying vec4 vColor;
varying vec2 vTextureCoord;

void main(void) {
	gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4(aVertexPosition, 1);
	vColor = vec4(aColor.rgb * aColor.a, aColor.a);
	vTextureCoord = vec2(aTextureCoord.x, 1.0 - aTextureCoord.y);
}
`;

const fragmentShader = `
/* NeutrinoParticles Fragment Shader (Normal, Add materials) */ 
precision mediump float;

varying vec4 vColor;
varying vec2 vTextureCoord;

uniform sampler2D uMainSampler;
uniform int uShaderType;

void main(void) {
	vec4 texel = texture2D(uMainSampler, vTextureCoord);
	if (uShaderType == 0)
	{
		gl_FragColor = vColor * texture2D(uMainSampler, vTextureCoord);
	}
	else
	{
		float alpha = vColor.a * texel.a;
		if (alpha > 0.01)
		{
			vec3 rgb = vColor.rgb * texel.rgb / alpha;
			gl_FragColor = vec4(mix(vec3(1, 1, 1), rgb, alpha), 1);
		}
		else
		{
			gl_FragColor = vec4(1, 1, 1, 1);
		}
	}
}
`;

export {
	vertexShader,
	fragmentShader
}