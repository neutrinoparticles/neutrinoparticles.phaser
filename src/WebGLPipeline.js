import * as Phaser from 'phaser';
import { vertexShader, fragmentShader } from './Shaders';

/**
 * Implements rendering pipeline for WebGL for NeutrinoParticles.
 * 
 * Usually you won't create objects of this class manually. The pipeline is added
 * to the renderer by {@link GamePlugin#init}.
 * 
 * It is used only for WebGL rendering.
 * 
 * @memberof PhaserNeutrino
 * @alias WebGLPipeline
 */

class WebGLPipeline extends Phaser.Renderer.WebGL.WebGLPipeline {
    constructor(config) {
        const { game, renderer } = config;
        const gl = renderer.gl;

        super({
            game,
            renderer,
            gl,
            topology: gl.TRIANGLES,
            vertShader: vertexShader,
            fragShader: fragmentShader,
            vertexCapacity: 6000,
            vertexSize: Float32Array.BYTES_PER_ELEMENT * 5 + Uint8Array.BYTES_PER_ELEMENT * 4,
            attributes: [
                {
                    name: 'aVertexPosition',
                    size: 3,
                    type: gl.FLOAT,
                    normalized: false,
                    offset: 0
                },
                {
                    name: 'aTextureCoord',
                    size: 2,
                    type: gl.FLOAT,
                    normalized: false,
                    offset: Float32Array.BYTES_PER_ELEMENT * 3
                },
                {
                    name: 'aColor',
                    size: 4,
                    type: gl.UNSIGNED_BYTE,
                    normalized: true,
                    offset: Float32Array.BYTES_PER_ELEMENT * 5
                }
            ]
        });

        // Ugly magic from Phaser. Inject Transform into the object.
        Object.assign(this, Phaser.Renderer.WebGL.Pipelines.ModelViewProjection);

        this.floats = new Float32Array(this.vertexData);
        this.uints = new Uint32Array(this.vertexData);

        this._currentTexture = null;
        this._currentBlendMode = null;

        this.mvpInit();
    }

    onPreRender() {
        super.onPreRender();
    }

    onRender(scene, camera) {
        super.onRender(scene, camera);
        const m3 = camera.matrix;

        // The rest of view matrix is left unchanged (identity).
        this.viewMatrix[0] = m3.a;
        this.viewMatrix[1] = m3.b;
        this.viewMatrix[4] = m3.c;
        this.viewMatrix[5] = m3.d;
        this.viewMatrix[12] = m3.e;
        this.viewMatrix[13] = m3.f;
        this.viewMatrixDirty = true;

        this._currentTexture = null;
        this._currentBlendModel = null;

        this.mvpUpdate();
    }

    onBind(effect) {
        super.onBind();

        this.setInt1('uMainSampler', 0);
        return this;
    }

    resize(width, height, resolution) {
        super.resize(width, height, resolution);
        this.projOrtho(0, this.width, this.height, 0, -1000.0, 1000.0);
        return this;
    }

    pushQuad(vertices, texture, blendMode) {
        let flushed = false;

        if (this.vertexCount + 6 > this.vertexCapacity) {
            this.flush();
            flushed = true;
        }

        if (texture != this._currentTexture) {
            if (this.vertexCount > 0)
                this.flush();

            this.renderer.setTexture2D(texture, 0, false);
            this._currentTexture = texture;
        }

        if (blendMode != this._currentBlendMode) {
            if (this.vertexCount > 0)
                this.flush();

            this.renderer.setBlendMode(blendMode);
            if (blendMode != Phaser.BlendModes.MULTIPLY)
                this.setInt1('uShaderType', 0);
            else
                this.setInt1('uShaderType', 1);

            this._currentBlendMode = blendMode;
        }

        let offset32 = this.vertexCount * this.vertexComponentCount;

        let copyVertex = (vertexIndex) => {
            const vertex = vertices[vertexIndex];
            this.floats.set(vertex.position, offset32);
            this.floats.set(vertex.texCoords, offset32 + 3);
            this.bytes.set(vertex.color, (offset32 + 5) * 4);

            offset32 += this.vertexComponentCount;
        }

        copyVertex(0);
        copyVertex(1);
        copyVertex(3);
        copyVertex(1);
        copyVertex(2);
        copyVertex(3);

        this.vertexCount += 6;
    }

    flush() {
        super.flush();
    }

}

export {
    WebGLPipeline
}