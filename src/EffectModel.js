/**
 * Represents NeutrinoParticles effect model in Phaser.
 * 
 * It actually wraps {@link neutrinoparticles.js}
 * exported effect model to perform all necessary Phaser-related integration.
 * 
 * Usually you won't create objects of this class manually, but rather use
 * {@link Phaser.Loader.LoaderPlugin.neutrino|scene.load.neutrino()} to load effects.
 * 
 * @memberof PhaserNeutrino
 * @alias EffectModel
 * 
 * @param {Phaser.Scene} scene The scene this effect is created for.
 * @param {string} scriptText The source code of the effect file.
 * @param {Object} [options={}] Options
 * @param {string[]} [options.atlases=[]] A list of keys of atlases where to make textures lookup.
 *        If a texture is found on the atlas, it will be used for rendering. Otherwise
 *        the texture will be loaded as standalone. So, it is important to have all
 *        necessary atlases fully loaded before loading effect model.
 * @alias EffectModel
 */
class EffectModel {

  constructor(scene, scriptText, options) {
    options = Object.assign({
      atlases: []
    }, options);

    /**
     * The NeutrinoParticles plugin instance.
     * @type {GamePlugin}
     */
    this.gamePlugin = scene.plugins.get('neutrino');

    /**
     * The scene this effect is loaded for.
     * @type {Phaser.Scene}
     */
    this.scene = scene;

    const evalScript = "(function(ctx) {\n" + scriptText +
      "\nreturn new NeutrinoEffect(ctx);\n})(this.gamePlugin.neutrino);";

    /**
     * {@link neutrinoparticles.js} effect model
     */
    this.neutrinoEffectModel = eval(evalScript);

    /**
     * Array of texture frames used in the effect.
     * @type {Phaser.Textures.Frame[]}
     */
    this.textureFrames = [];

    this.texturesRemap = [];
    
    this.textureImageDescs = [];
    this._numTexturesToLoadLeft = 0;

    this._startLoadTextures(options.atlases);
  }

  /**
   * If the effect model is ready to be used in simulation and rendering.
   * Basically it means that all textures used in the effect are successfully loaded.
   * @type {boolean}
   */
  get ready() {
    return this._numTexturesToLoadLeft === 0;
  }

  _startLoadTextures(atlases) {
    const numTextures = this.neutrinoEffectModel.textures.length;
    this._numTexturesToLoadLeft = numTextures;

    for (let imageIndex = 0; imageIndex < numTextures; ++imageIndex) {
      const textureName = this.neutrinoEffectModel.textures[imageIndex];
      const texturePath = this.gamePlugin.texturesBasePath + textureName;
      const texturePathNoExt = noext(texturePath);

      // Looking for texture in provided atlases.
      let frame = this._findFrameInAtlases(atlases, textureName);
      if (!frame)
        frame = this._findFrameInAtlases(atlases, noext(textureName));

      // If we don't have frame on atlases, try to load texture by full path.
      if (!frame && this.scene.sys.textures.exists(texturePathNoExt))  {
        const texture = this.scene.sys.textures.get(texturePathNoExt);
        frame = texture.frames[texture.firstFrame];
      }

      if (frame) {
        this._onTextureLoaded(imageIndex, frame);
      }
      else {
        this.scene.load.on('filecomplete-image-' + texturePathNoExt,
          (function (_this, imageIndex) {
            return function (key, type, data) {
              _this._onTextureLoaded(imageIndex, data.frames[data.firstFrame]);
            }
          })(this, imageIndex))

        this.scene.load.image(texturePathNoExt, texturePath);
      }
    }
  }

  _findFrameInAtlases(atlases, frameName) {
    for (let i = 0; i < atlases.length; ++i) {
      const key = atlases[i];
      if (!this.scene.sys.textures.exists(key))
        continue;

      const texture = this.scene.sys.textures.get(key);
      const frame = texture.frames[frameName];
      if (frame)
        return frame;
    }

    return null;
  }

  _onTextureLoaded(index, frame) {
    this.textureFrames[index] = frame;

    this._numTexturesToLoadLeft--;

    if (this.gamePlugin.canvasRenderer) {
      this.textureImageDescs[index] = new this.gamePlugin.neutrino.ImageDesc(
        frame.source.image, frame.u0 * frame.source.width, 
        frame.v0 * frame.source.height, frame.width, frame.height);
    }

    if (this._numTexturesToLoadLeft === 0) {

      if (!this.gamePlugin.canvasRenderer) {
        this._initTexturesRemapIfNeeded();
      }
    }
  }

  _fullCover(frame) {
    return frame.width === frame.source.width && frame.height === frame.source.height;
  }

  _initTexturesRemapIfNeeded() {
    let remapNeeded = false;

    for (var texIdx = 0; texIdx < this.textureFrames.length; ++texIdx) {
      //checks if its an atlas subtexture
      if (!this._fullCover(this.textureFrames[texIdx])) {
        remapNeeded = true;
        break;
      }
    }

    if (remapNeeded) {
      for (let texIdx = 0; texIdx < this.textureFrames.length; ++texIdx) {
        const frame = this.textureFrames[texIdx];

        this.texturesRemap[texIdx] = new this.gamePlugin.neutrino.SubRect(
          frame.u0, frame.v0, frame.u1 - frame.u0, frame.v1 - frame.v0);
      }
    }
  }
}

function noext(path) {
  return path.replace(/\.[^/.]+$/, ""); // https://stackoverflow.com/a/4250408
}

export {
  EffectModel
}