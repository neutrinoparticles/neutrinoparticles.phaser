import * as Neutrino from 'neutrinoparticles.js';
import { WebGLPipeline } from './WebGLPipeline';

/**
 * Main NeutrinoParticles plugin class.
 * 
 * Represents a shared object (or context) for all effects in the game.
 * 
 * Provides functionality to download or generate noise (turbulence) for effects.
 *
 * Usually you won't create objects of this class manually, rather use
 * {@link installPlugin} to install the plugin to the game.
 * 
 * When WebGL rendering is used, this plugin on {@link GamePlugin#init} will add
 * {@link WebGLPipeline} to the renderer.
 * 
 * @memberof PhaserNeutrino
 * @alias GamePlugin
 * 
 * @example
 * // Install plugin to the game
 * PhaserNeutrino.installPlugin(game, {});
 * 
 * @example
 * // Later, the plugin can be accessed from PluginManager
 * game.plugins.get('neutrino');
 * // or
 * scene.plugins.get('neutrino');
 * 
 * @param {Phaser.Plugins.PluginManager} pluginManager Plugin manager of the game.
 */

class GamePlugin {
  constructor(pluginManager) {
    /**
     * The {@link Phaser.Plugins.PluginManager} instance.
     */
    this.pluginManager = pluginManager;
  }

  /**
   * Initializes the plugin.
   * 
   * This method is called automatically by Phaser when this plugin is registered by
   * {@link installPlugin}
   * 
   * @param {Object} options
   * @param {string} [options.texturesBasePath=""] A path that will be prefixed to all 
   *        textures described in loaded effects. For atlases textures lookup,
   *        a path without this prefix will be used. Will init {@link GamePlugin#texturesBasePath}.
   * @param {boolean} [options.generateNoise=false] If true, {@link GamePlugin#generateNoise} will
   *        be called to generate noise texture right after the plugin initialized. Please note,
   *        it will block script execution until finished.
   */
  init(options) {

    options = Object.assign({
      texturesBasePath: "",
      generateNoise: false
    }, options);

    /**
     * The game instance.
     * @type {Phaser.Game}
     */
    this.game = this.pluginManager.game;

    /**
     * The renderer instance.
     * @type {Phaser.Renderer.Canvas.CanvasRenderer|Phaser.Renderer.WebGL.WebGLRenderer}
     */
    this.renderer = this.game.renderer;

    /**
     * Shows if the canvas renderer is in use.
     * @type {boolean}
     */
    this.canvasRenderer = (Phaser.Renderer.Canvas &&
      Phaser.Renderer.Canvas.CanvasRenderer &&
      this.renderer instanceof Phaser.Renderer.Canvas.CanvasRenderer);

    if (!this.canvasRenderer) {
      this.renderer.addPipeline('NeutrinoParticles',
        new WebGLPipeline({ game, renderer: game.renderer }));
    }

    /**
     * A path that will be prefixed to all textures described in loaded effects.
     * For atlases textures lookup, a path without this prefix will be used.
     * @type {string}
     */
    this.texturesBasePath = options.texturesBasePath;

    /**
     * The main {@link neutrinoparticles.js}
     * context.
     * @type {Neutrino.Context}
     */
    this.neutrino = new Neutrino.Context();

    /**
     * If noise already initialized in a some way.
     * @member {boolean}
     * @private
     */
    this._noiseInitialized = false;

    /**
     * In case if noise is being generated in multiple steps by {@link GamePlugin#generateNoiseStep},
     * this will store Neutrino.Context.NoiseGenerator until the process is finished.
     * @member {Neutrino.Context.NoiseGenerator}
     * @private
     */
    this._noiseGenerator = null;

    if (options.generateNoise)
      this.generateNoise();
  }

  /**
   * Called when the plugin needs to start. Do nothing.
   */
  start() {
  }

  /**
   * Called when the plugin needs to stop. Do nothing.
   */
  stop() {
  }

  /**
   * Destroys the plugin.
   */
  destroy() {
    this.neutrino = null;
  }

  /**
     * Initiates loading of pre-generated noise file. This file you can find
     * in repository of 
     * {@link neutrinoparticles.js} 
     * library. You will need to deploy this file with your application.
     * 
     * @param {string} path Path to the directory where 'neutrinoparticles.noise.bin' file
     * is located.
     * @param {function()} success Success callback.
     * @param {function()} fail Fail callback.
     */
  loadNoise(path, success, fail) {
    if (this._noiseInitialized) {
      if (success) success();
      return;
    }

    this.neutrino.initializeNoise(path,
      function () {
        this._noiseInitialized = true;
        if (success) success();
      },
      fail);
  }

  /**
   * Generates noise for effects in a single call. 
   *  
   * This method should be called only once at the start of the game.
   * It will lock script executing until finished.
   */
  generateNoise() {
    if (this._noiseInitialized)
      return;

    let noiseGenerator = new this.neutrino.NoiseGenerator();
    while (!noiseGenerator.step()) { // approx. 5,000 steps
      // you can use 'noiseGenerator.progress' to get generating progress from 0.0 to 1.0
    }

    this._noiseInitialized = true;
  }

  /**
   * Generates noise in multiple steps. With this function you can spread noise
   * generation over multiple frames.
   * 
   * Generating noise should be made only once in the start of the game.
   * 
   * @example
   * let neutrino = game.plugins.get('neutrino');
   * let result;
   * do {
   *   result = neutrino.generateNoiseStep();
   *   const progress = result.progress; // Progress in range [0; 1]
   * 
   *   // do something with progress
   *   
   * } while (!result.done);
   */
  generateNoiseStep() {
    if (this._noiseInitialized) {
      return { done: true, progress: 1.0 };
    }

    if (!this._noiseGenerator)
      this._noiseGenerator = new this.neutrino.NoiseGenerator();

    const result = this._noiseGenerator.step();
    const _progress = this._noiseGenerator.progress;

    if (result) {
      this._noiseInitialized = true;
      this._noiseGenerator = null;
    }

    return { done: result, progress: _progress };
  }
}

export {
  GamePlugin
}