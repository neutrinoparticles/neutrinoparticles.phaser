import { WebGLPipeline } from './WebGLPipeline';
import { File } from './File';
import { GamePlugin } from './GamePlugin';
import { Effect } from './Effect';
import { EffectModel } from './EffectModel';

/**
 * @namespace PhaserNeutrino
 */

/**
 * @namespace neutrinoparticles.js
 * @see {@link https://gitlab.com/neutrinoparticles/neutrinoparticles.js/|neutrinoparticles.js} documentation.
 */

/**
 * @namespace Phaser.Loader.LoaderPlugin
 * @see {@link https://photonstorm.github.io/phaser3-docs/Phaser.Loader.LoaderPlugin.html|Phaser.Loader.LoaderPlugin} API Reference
 */

/**
 * @namespace Phaser.GameObjects.GameObjectFactory 
 * @see {@link https://photonstorm.github.io/phaser3-docs/Phaser.GameObjects.GameObjectFactory.html|Phaser.GameObjects.GameObjectFactory} API Reference
 */

/**
  * @namespace Phaser.GameObjects.GameObjectCreator
  * @see {@link https://photonstorm.github.io/phaser3-docs/Phaser.GameObjects.GameObjectCreator.html|Phaser.GameObjects.GameObjectCreator} API Reference
  */
 
/**
 * @namespace Phaser.GameObjects.GameObject
 * @see {@link https://photonstorm.github.io/phaser3-docs/Phaser.GameObjects.GameObject.html|Phaser.GameObjects.GameObject} API Reference.
 */

/**
 * Loads NeutrinoParticles effect.
 * 
 * This is a NeutrinoParticles extension of Phaser's loader.
 * This function will request for load an effect file. After the file is loaded,
 * {@link EffectModel} will be created and stored in binaries cache
 * under provided key.
 *  
 * @function neutrino
 * @memberof Phaser.Loader.LoaderPlugin
 * 
 * @example
 * scene.load.neutrino('water_stream', 'export_js/water_stream.js');
 * 
 * @param {string} key A key to store loaded effect model in binaries cache.
 * @param {string} url A URL to load effect from.
 * @param {Object} [options={}] Options that will be passed to the constructor of 
 *   {@link EffectModel}.
 * @param {Phaser.Types.Loader.XHRSettingsObject} [xhrSettings] Extra XHR Settings specifically for this file.
 */
Phaser.Loader.FileTypesManager.register('neutrino',
    function (key, url, options, xhrSettings) {
        // this === Phaser.Loader

        const file = new File(this, { key, url, options, xhrSettings });
        this.addFile(file);
    });

function makeEffect(key, options, addToScene, addToUpdate) {
    const effectModel = this.scene.sys.cache.binary.get(key);
    const effect = new Effect(effectModel, this.scene, options);

    if (addToScene)
        this.displayList.add(effect);

    if (addToUpdate)
        this.updateList.add(effect);

    return effect;
}

/**
 * Adds instance of NeutrinoParticles effect to the scene.
 * 
 * The function will create instance of {@link Effect} and
 * add it to the display list of the scene. By default it adds the effect
 * also in update list of the scene.
 *  
 * @function neutrino
 * @memberof Phaser.GameObjects.GameObjectFactory
 * 
 * @example
 * let effect = scene.make.neutrino('some_effect', {
 *   position: [400, 300, 0],
 *   scale: 2
 * });
 * 
 * @param {string} key A key of effect, loaded by {@link Phaser.Loader.LoaderPlugin.neutrino}.
 * @param {Object} [config={}] Options that will be passed to the constructor of 
 *        {@link Effect}.
 * @param {boolean} [addToUpdate=true] Add created effect instance to the update list.
 *        If you don't add effect to the update list, you will need to call
 *        {@link Effect#preUpdate} manually to update it.
 * @returns {Effect} Created instance of effect.
 */
Phaser.GameObjects.GameObjectFactory.register('neutrino',
    function (key, config, addToUpdate) {
        if (addToUpdate === undefined)
            addToUpdate = true;

        return makeEffect.call(this, key, config, true, addToUpdate);
    });

/**
 * Creates instance of NeutrinoParticles effect.
 * 
 * The function will create instance of {@link Effect} and
 * add it to the display list and update list if necessary.
 *  
 * @function neutrino
 * @memberof Phaser.GameObjects.GameObjectCreator
 * 
 * @example
 * let effect = scene.make.neutrino('some_effect', {
 *   position: [400, 300, 0],
 *   scale: 2
 * });
 * 
 * @param {string} key A key of effect, loaded by {@link Phaser.Loader.LoaderPlugin.neutrino}.
 * @param {Object} [config={}] Options that will be passed to the constructor of 
 *        {@link Effect}.
 * @param {boolean} [addToScene=false] Add created effect instance to the scene.
 * @param {boolean} [addToUpdate=true] Add created effect instance to the update list.
 *        If you don't add effect to the update list, you will need to call
 *        {@link Effect#preUpdate} manually to update it.
 * @returns {Effect} Created instance of effect.
 */
Phaser.GameObjects.GameObjectCreator.register('neutrino',
    function (key, options, addToScene, addToUpdate) {
        if (addToScene === undefined)
            addToScene = false;

        if (addToUpdate === undefined)
            addToUpdate = true;

        return makeEffect.call(this, key, options, addToScene, addToUpdate);
    });


/**
 * Installs NeutrinoParticles plugin to the game.
 * 
 * This will create {@link GamePlugin} and install it to the game.
 * The plugin will be automatically started.
 * 
 * You have to call this function right after the game instance is created and before
 * any NeutrinoParticles effects are loaded.
 * 
 * @memberof PhaserNeutrino
 * @alias installPlugin
 * 
 * @example
 * const game = new Phaser.Game({
 *   scene: {
 *     preload: preload,
 *     create: create
 *   }
 * });
 * 
 * // Install NeutrinoParticles plugin.
 * PhaserNeutrino.installPlugin(game);
 * 
 * function preload() {
 *   // Load effect.
 *   this.load.neutrino('some_effect', 'path/to/some_effect.js');
 * }
 * 
 * function create() {
 *   // Add effect instance to the scene.
 *   this.add.neutrino('some_effect');
 * }
 * 
 * @param {Phaser.Game} game The game object.
 * @param {Object} [options={}] Options that will be passed to the 
 *        {@link GamePlugin#init}.
 */

function installPlugin(game, options) {
    game.plugins.install('neutrino', GamePlugin, true, null, options || {});
}

export * from './const'

export {
    Effect,
    EffectModel,
    GamePlugin,
    WebGLPipeline,
    File,
    installPlugin
}


