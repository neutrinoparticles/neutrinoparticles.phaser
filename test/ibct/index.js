import {IBCT} from './ibct.js';
import commandLineArgs from 'command-line-args';
import { TestAtlases } from './atlases'
import { TestPositioning } from './positioning'
import { TestRotating } from './rotating'
import { TestScaling } from './scaling'
import { TestSceneHierarchy } from './scene_hierarchy'
import { TestSimulation } from './simulation'
import { Test10KParticles } from './10k_particles.js';
import { TestCameras } from './cameras.js';

const ctx = {
  ibct: null
}

describe('IBCT', function() {

  beforeEach(function() {

    const optionDefinitions = [
      { name: 'ibct-expect-dir', type: String, defaultOption: "__expect__" },
    ];

    const options = commandLineArgs(optionDefinitions, { partial: true });

    return new Promise((resolve) => {
      ctx.ibct = new IBCT({
         expectDir: options["ibct-expect-dir"] 
        }, resolve);
    });
  });

  afterEach(function() {
    ctx.ibct.destroy();
    ctx.ibct = null;
  });

  function TestEverything(ctx)
  {
    Test10KParticles(ctx);
    TestAtlases(ctx);
    TestCameras(ctx);
    TestPositioning(ctx);
    TestRotating(ctx);
    TestScaling(ctx);
    TestSceneHierarchy(ctx);
    TestSimulation(ctx);
  };

  describe('WebGL', function() {
    beforeEach(function() {
      ctx.ibct.setDefaultResetOptions({type: 2}); // WebGL == 2
    });

    TestEverything(ctx);
  });

  describe('Canvas', function() {
    beforeEach(function() {
      ctx.ibct.setDefaultResetOptions({type: 1 }); // Canvas == 1
    });

    TestEverything(ctx);
  });

});
  
