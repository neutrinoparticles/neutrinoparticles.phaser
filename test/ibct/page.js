const ipc = require('electron').ipcRenderer;
const path = require('path');

window.onerror = function(error)
{
  ipc.send('error', error);
}

let game, test = {};

function initialize(config, _preload, _create) {
  config = Object.assign({
    width: 800,
    height: 600,
    type: Phaser.AUTO,
    scene: {
      preload: _preload,
      create: function () {
        _create.call(this);
        game.loop.raf.stop();
        window.requestAnimationFrame(() => {
          ipc.send('effects-ready');
        });
      }
    },
    backgroundColor: 0x404040
  }, config || {});

  if (game)
    destroy();

  game = new Phaser.Game(config);

  const neutrinoConfig = Object.assign({
    texturesBasePath: "textures/",
    generateNoise: false
  }, config.neutrino || {});

  PhaserNeutrino.installPlugin(game, neutrinoConfig);
}

function destroy() {
  const canvas = (Phaser.Renderer.Canvas &&
    Phaser.Renderer.Canvas.CanvasRenderer && 
    game.renderer instanceof Phaser.Renderer.Canvas.CanvasRenderer);

  game.destroy(true);
  // Update game two times only for WebGLRenderer,
  // as after one time we have a black screen on build bot
  // on the next test.
  step(0);
  if (!canvas)
    step(0);
  game = null;
  test = {};
}

function step(sec) {
  game.step(window.performance.now(), sec * 1000.0);
}

function check() {
  return game.canvas.toDataURL();
}