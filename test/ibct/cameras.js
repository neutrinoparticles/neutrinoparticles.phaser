export function TestCameras(ctx) {
    describe('Cameras', function (cb) {

        it("OffsetAndZoom", function (cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {},
                `function() {
                    this.load.neutrino('effect', 'effects/cameras.js');
                }`,
                `function() {
                    test.effect = this.add.neutrino('effect', {
                        position: [400, 300, 0],
                        scale: 2
                    });

                    this.cameras.main.centerOn(0, 0);
                    this.cameras.main.setZoom(0.5);

                }`, runTest, cb);

            function runTest() {
                ibct.stepAndCheck(4);
                ibct.finalize(cb);
            }
        });
    });
}
