export function TestPositioning(ctx) {
    describe('Positioning', function (cb) {

        it("Static", function (cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {},
                `function() {
                    this.load.neutrino('effect', 'effects/positioning.js');
                }`,
                `function() {
                    test.effect = this.add.neutrino('effect', {
                        position: [400, 300, 0]
                    });
                }`, runTest, cb);

            function runTest() {
                ibct.stepAndCheck(0);
                ibct.stepAndCheck(4);
                ibct.finalize(cb);
            }
        });

        it("Moving", function (cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {},
                `function() {
                    this.load.neutrino('effect', 'effects/positioning.js');
                }`,
                `function() {
                    test.effect = this.add.neutrino('effect', {
                        position: [0, 0, 0]
                    });
                }`, runTest, cb);

            function runTest() {
                ibct.stepAndCheck(0);
                ibct.exec(`test.effect.setPosition(400, 300);`);
                ibct.stepAndCheck(2);
                ibct.finalize(cb);
            }
        });

        it("MovingAndJump", function (cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {},
                `function() {
                    this.load.neutrino('effect', 'effects/positioning.js');
                }`,
                `function() {
                    test.effect = this.add.neutrino('effect', {
                        position: [0, 0, 0]
                    });
                }`, runTest, cb);

            function runTest() {
                ibct.stepAndCheck(0);
                ibct.exec(`test.effect.setPosition(200, 150);`);
                ibct.stepAndCheck(1);
                ibct.exec(`
                    test.effect.resetPosition({ position: [400, 300, 0] });
                    test.effect.setPosition(600, 450);`);
                ibct.stepAndCheck(1);
                ibct.finalize(cb);
            }
        });
    });
}
