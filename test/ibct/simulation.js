import * as path from 'path';
import * as fs from 'fs';

function fsFriendly(p) {
    return p.replace(/[^a-z0-9]/gi, '_').toLowerCase();
}

export function TestSimulation(ctx)
{
    function TestBySimpleSimulationFromDir(relEffectsDir, resetOptions)
    {
        const effectsDir = path.join(__dirname, relEffectsDir);
        const effectsFiles = fs.readdirSync(effectsDir);

        for (let i = 0; i < effectsFiles.length; ++i) {
            const effectFile = path.join(effectsDir, effectsFiles[i]);

            if (fs.statSync(effectFile).isDirectory())
            continue;

            const testTitle = fsFriendly(path.join(relEffectsDir, effectsFiles[i]));

            it (testTitle, function(cb) {
                const ibct = ctx.ibct;

                ibct.reset(this.test.fullTitle(), resetOptions,
                    `function() {
                        this.load.neutrino('effect', '` + path.join(relEffectsDir, effectsFiles[i]) + `');
                    }`,
                    `function() {
                        test.effect = this.add.neutrino('effect', {
                            position: [400, 300, 0]
                        });
                    }`, runTest, cb);

                function runTest() {
                    const updates = [0.1, 0.2, 0.3, 0.4, 5];
                    updates.forEach((dt) => {
                        ibct.stepAndCheck(dt);
                    });
                    
                    ibct.finalize(cb);
                }
            });
        }
    }

    describe('Simulation', function(cb) {
        TestBySimpleSimulationFromDir("effects/simulation", { neutrino: { generateNoise: false }});
        TestBySimpleSimulationFromDir("effects/turbulence", { neutrino: { generateNoise: true }});
    });
}
