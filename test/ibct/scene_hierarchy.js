export function TestSceneHierarchy(ctx)
{
    describe('SceneHierarchy', function(cb) {

        it("StaticParent", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {},
                `function() {
                    this.load.neutrino('effect', 'effects/scene_hierarchy.js');
                }`,
                `function() {
                    test.effect = this.add.neutrino('effect', {
                        position: [0, 0, 0]
                    });
                    test.container = this.add.container(400, 300);
                    test.container.add(test.effect);
                }`, runTest, cb);

            function runTest() {
                ibct.stepAndCheck(2);
                ibct.finalize(cb);
            }
        });

        it("RotatingParentAndChild", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {},
                `function() {
                    this.load.neutrino('effect', 'effects/scene_hierarchy.js');
                }`,
                `function() {
                    test.effect = this.add.neutrino('effect', {
                        position: [150, 0, 0]
                    });
                    test.container = this.add.container(400, 300);
                    test.container.add(test.effect);
                }`, runTest, cb);

            function runTest() {
                ibct.stepAndCheck(0);
                ibct.exec(`
                    test.container.angle = 45;
                    test.effect.angle = 45;`);
                ibct.stepAndCheck(2);
                ibct.finalize(cb);
            }
        });
        
    });
}
