export function Test10KParticles(ctx) {
    it("10K", function (cb) {
        const ibct = ctx.ibct;

        ibct.reset(this.test.fullTitle(), {},
            `function() {
                this.load.neutrino('effect', 'effects/10k_particles.js');
            }`,
            `function() {
                test.effect = this.add.neutrino('effect', {
                    position: [400, 300, 0]
                });
            }`, runTest, cb);

        function runTest() {
            ibct.stepAndCheck(1);
            ibct.finalize(cb);
        }
    });
}
