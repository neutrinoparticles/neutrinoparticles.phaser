export function TestRotating(ctx)
{
    describe('Rotating', function(cb) {

        function TestStaticRotating(angle, cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {},
                `function() {
                    this.load.neutrino('effect', 'effects/rotating.js');
                }`,
                `function() {
                    test.effect = this.add.neutrino('effect', {
                        position: [400, 300, 0],
                        angle: ` + String(angle) + `
                    });
                }`, runTest, cb);

            function runTest() {
                ibct.stepAndCheck(2);
                ibct.finalize(cb);
            }
        }

        // Static*
        {
            const angles = [ 45, 90, 135, 180, 225, 270, 315, -45, -90, -135, -180, -225, -270, -315 ];

            angles.forEach((angle) => {
                it("Static" + String(angle), function(cb) {
                    TestStaticRotating.call(this, angle, cb);    
                });
            });
        }

        function TestRotatingByAngle(angle, cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {},
                `function() {
                    this.load.neutrino('effect', 'effects/rotating.js');
                }`,
                `function() {
                    test.effect = this.add.neutrino('effect', {
                        position: [400, 300, 0]
                    });
                }`, runTest, cb);

            function runTest() {
                ibct.stepAndCheck(0);
                ibct.exec(`test.effect.angle = ` + String(angle));
                ibct.stepAndCheck(2);
                ibct.finalize(cb);
            }
        }

        it("DynamicRight90", function(cb) {
            TestRotatingByAngle.call(this, 90, cb);
        });

        it("DynamicLeft90", function(cb) {
            TestRotatingByAngle.call(this, -90, cb);
        });

        it("Spiral", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {},
                `function() {
                    this.load.neutrino('effect', 'effects/rotating.js');
                }`,
                `function() {
                    test.effect = this.add.neutrino('effect', {
                        position: [400, 300, 0]
                    });
                }`, runTest, cb);

            function runTest() {
                ibct.stepAndCheck(0);
                ibct.exec(`test.effect.angle = 90`);
                ibct.stepAndCheck(0.25);
                ibct.exec(`test.effect.angle = 180`);
                ibct.stepAndCheck(0.25);
                ibct.exec(`test.effect.angle = 270`);
                ibct.stepAndCheck(0.25);
                ibct.exec(`test.effect.angle = 360`);
                ibct.stepAndCheck(0.25);
                ibct.finalize(cb);
            }
        });

        it("RotateResetRotate", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {},
                `function() {
                    this.load.neutrino('effect', 'effects/rotating.js');
                }`,
                `function() {
                    test.effect = this.add.neutrino('effect', {
                        position: [400, 300, 0]
                    });
                }`, runTest, cb);

            function runTest() {
                ibct.stepAndCheck(0);
                ibct.exec(`test.effect.angle = 45`);
                ibct.stepAndCheck(1);
                ibct.exec(`
                    test.effect.resetPosition({ angle: 90 }); 
                    test.effect.angle = 180;`);
                ibct.stepAndCheck(1);
                ibct.stepAndCheck(1);
                    
                ibct.finalize(cb);
            }
        });
    });
}
