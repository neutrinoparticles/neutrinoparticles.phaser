export function TestScaling(ctx)
{
    describe('Scaling', function(cb) {
        it("Uniform", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {},
                `function() {
                    this.load.neutrino('effect', 'effects/scaling.js');
                }`,
                `function() {
                    test.effect1 = this.add.neutrino('effect', {
                        position: [100, 100, 0],
                        scale: 1
                    });
                    test.effect2 = this.add.neutrino('effect', {
                        position: [600, 200, 0],
                        scale: 2
                    });
                    test.effect3 = this.add.neutrino('effect', {
                        position: [300, 400, 0],
                        scale: 4
                    });
                }`, runTest, cb);

            function runTest() {
                ibct.stepAndCheck(2);
                ibct.finalize(cb);
            }
        });
    });
}
