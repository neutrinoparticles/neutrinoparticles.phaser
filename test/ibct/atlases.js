export function TestAtlases(ctx) {
    describe('Atlases', function (cb) {

        it("Simple", function (cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {},
                `function() {
                    this.load.atlas('test_atlas', 'textures/atlas.png', 'textures/atlas.json');

                    this.load.once('filecomplete', () => {
                        this.load.neutrino('effect', 'effects/atlas/2emitters.js',
                            { atlases: ['test_atlas'] });
                    });
                }`,
                `function() {
                    test.effect = this.add.neutrino('effect', {
                        position: [400, 300, 0]
                    });
                }`, runTest, cb);

            function runTest() {
                ibct.stepAndCheck(4);

                ibct.finalize(function(err) {
                    if (err) {
                        cb(err);
                        return;
                    }

                    // Check that there are no standalone textures loaded. All of them
                    // should be taken from atlas.
                    ibct.exec(`
                        const textures = game.scene.scenes[0].sys.textures;
                        return !textures.exists('textures/test1')
                            && !textures.exists('textures/test2');
                    `).then(function(result) {
                        if (result)
                            cb();
                        else
                            cb("Loaded standalone textures!");
                    })
                });
            }
        });
    });
}
