# neutrinoparticles.phaser

The package allows you to render [NeutrinoParticles](https://neutrinoparticles.com/) effects in [Phaser 3](https://phaser.io/) game engine.

Phaser 2 is not supported.

## API documentation

For more info about latest API you can check [API documentation](https://neutrinoparticles.gitlab.io/neutrinoparticles.phaser-doc/master/) pages.

For specific library version refer to [Releases](https://gitlab.com/neutrinoparticles/neutrinoparticles.phaser/-/releases) page.

## Issues and Requests
In you have any request or issue, please, feel free to post it on [Issues](issues) page. It will be considered ASAP.

## Installation

You can install the package with `npm`:
```
> npm install neutrinoparticles.phaser
```
Or download pre-built package at [Releases](https://gitlab.com/neutrinoparticles/neutrinoparticles.phaser/-/releases) page. There are [UMD](https://www.davidbcalhoun.com/2014/what-is-amd-commonjs-and-umd/) packages which you can use in any environment.

## Quick usage

Acquire a package object. Depending on your environment:

* HTML
```html
<script src="path/to/neutrinoparticles.pixi/dist/neutrinoparticles.phaser.umd.js"></script>
```
* node.js
```javascript
var PhaserNeutrino = require('neutrinoparticles.phaser');
```
* ES6
```javascript
import * as PhaserNeutrino from 'neutrinoparticles.phaser';
```

Create game instance with options on your choise:
```javascript
const game = new Phaser.Game({
    width: 800,
    height: 600,
    type: Phaser.AUTO,
    scene: {
        preload: preload,
        create: create
    }
});
```

Install NeutrinoParticles plugin for the game:
```javascript
PhaserNeutrino.installPlugin(game, {
    texturesBasePath: "textures/",   // Prefix for textures
    generateNoise: true              // Generate noise to allow turbulence in effects
});
```

Load effect:
```javascript
function preload() {
    this.load.neutrino('water_stream', 'export_js/water_stream.js');
}
```

Add effect to the scene:
```javascript
function create() {
    this.add.neutrino('water_stream', {
        position: [400, 300, 0],
        scale: 2,
        angle: -45
    });
}
```

## Samples
There are several samples in folder `/samples` which you can refer to.

#### Running samples
To run the samples you will need web server running on localhost:80 with a web root on the root repository folder. If you have Python3 installed you can use `/scripts/start_http_server.sh` script to start it.

After server is running you can access the samples by `http://localhost/samples/<sample_name>.html`
> For different web server configuration, please adjust the path above accordingly.

#### Debug Samples with VSCode

You can use following launch configuration to start debugging of samples:
```json
{
    "name": "Samples in Chrome",
    "type": "chrome",
    "runtimeExecutable": "/path/to/chrome",
    "runtimeArgs": [
        "--incognito",
        "--new-window",
        "--remote-debugging-port=9222",
        "--user-data-dir=remote-profile"
    ],
    "request": "launch",
    "url": "http://localhost/samples/basic_usage.html",
    "webRoot": "${workspaceFolder}",
    "breakOnLoad": true,
    "sourceMaps": true,
    "port": 9222,
}
```
> To be able to set breakpoint in .html files you will need to enable `Allow Breakpoints Everywhere` flag in VSCode Settings.

## Tests
#### Running tests

To run tests you can use `npm run test` command as usual. It will start IBCT (image based comparison tests) in WebGL and Canvas modes. You will need to provide GPU environment to run the tests. It can be hardware video card or software renderer.

#### Debug with VSCode

You can use following launch configuration to start debugging of main thread of electron-mocha application which perform the tests:

```json
{
	"name": "Debug Tests",
	"type": "node",
	"request": "launch",
	"cwd": "${workspaceRoot}",
	"runtimeExecutable": "${workspaceRoot}/node_modules/.bin/electron-mocha",
	"windows": {
	  "runtimeExecutable": "${workspaceRoot}/node_modules/.bin/electron-mocha.cmd"
	},
	"program": "${workspaceRoot}/test/test.js",
	"args": [
		"--require", "@babel/register",
		"--timeout", "999999",
		"--colors",
		"--ibct-expect-dir", "__my_expect__",
		"test/test.js"
	],
	"console": "integratedTerminal",
	"protocol": "inspector",
}
```
> To be able to set breakpoint in .html files you will need to enable `Allow Breakpoints Everywhere` flag in VSCode Settings.

Please note, that `ibct-expect-dir` argument overrides default expectation files directory `__expect__` for IBCT. The directory is placed in /test/ibct. Such behaviour is useful when running tests locally, because different hardware gives different results and default expectations will most probably fail for you.

## Contribution
We will be much appreciated for any fix/feature merge requests and will consider it ASAP.

There are not much rules for such requests, just be sure that all tests are passing.