import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import babel from 'rollup-plugin-babel';
import pkg from './package.json';

export default [
	{
		external: ['phaser', 'neutrinoparticles.js'],
		input: 'src/main.js',
		output: {
			name: 'PhaserNeutrino',
			file: pkg.browser,
			format: 'umd',
			globals: {
				'phaser': 'Phaser',
				'neutrinoparticles.js': 'Neutrino'
			}
		},
		plugins: [
			resolve(),
			commonjs(),
			babel({
				exclude: ['node_modules/**']
			})
		]
	},
	{
		input: 'src/main.js',
		external: ['phaser', 'neutrinoparticles.js'],
		output: [
			{ file: pkg.main, format: 'cjs' },
			{ file: pkg.module, format: 'es' }
		],
		plugins: [
			babel({
				exclude: ['node_modules/**']
			})
		]
	}
];
